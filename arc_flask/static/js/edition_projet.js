let date = 0;
for (let i = 1; i < 6; i++) {
    date[i] = 0;
}
let initialisation = false;

let est_plus_recente = function (date1, date2) {
    if (date1[0] > date2[0]) {
        return false;
    } else if (date1[0] == date2[0]) {
        if (date1[1] > date2[1]) {
            return false;
        } else if (date1[1] == date2[1]) {
            if (date1[2] > date2[2]) {
                return false;
            } else if (date1[2] == date2[2]) {
                if (date1[3] > date2[3]) {
                    return false;
                } else if (date1[3] == date2[3]) {
                    if (date1[4] > date2[4]) {
                        return false;
                    } else if (date1[4] == date2[4]) {
                        if (date1[5] > date2[5]) {
                            return false;
                        } else if (date1[5] == date2[5]) {
                            return false;
                        }
                    }
                }
            }
        }
    }

    return true;
};

let error = null;

let afficherMsg = function (idProj) {
    $.ajax(
        {
            url: "/projet/get_messages/" + idProj,
            type: "GET",
            async: true,
            dataType: "json",
            success: function (sugg) {
                if (error == null || error) {
                    error = false;
                    $(".panel-heading").text("Commentaires")
                }

                for (let i = 0; i < Object.keys(sugg).length; i++) {
                    dateCur = [sugg[i].annee, sugg[i].mois, sugg[i].jour, sugg[i].heure, sugg[i].minute, sugg[i].seconde];
                    if (!initialisation || est_plus_recente(date, dateCur)) {
                        $('#messagePanel').append($('<div>').addClass("div" + i));
                        $('#messagePanel .div' + i).addClass("row message-bubble " + i).append($('<p>').text(sugg[i].fname + " " + sugg[i].lname).addClass("text-muted")).append($('<span>').text(sugg[i].Contenu));
                        // $('.row message-bubble '+i).append('<p>')
                        $('.row message-bubble ' + i + ' p').addClass("text-muted");
                        if (i == Object.keys(sugg).length - 1) {
                            dateCur = [sugg[i].annee, sugg[i].mois, sugg[i].jour, sugg[i].heure, sugg[i].minute, sugg[i].seconde];
                            if (date == null) {
                                date = dateCur;
                            } else {
                                // if(date != dateCur)
                                if (date[0] != dateCur[0] || date[1] != dateCur[1] || date[2] != dateCur[2] || date[3] != dateCur[3] || date[4] != dateCur[4] || date[5] != dateCur[5]) {
                                    scrollDown();
                                    date = dateCur;
                                }
                            }
                        }
                    }
                }
                initialisation = true;
            },
            error: function (req, status, err) {
                if (error == false) {
                    error = true;
                    $(".panel-heading").text("Commentaires - Impossible de récuperer les suggestions.")
                }
            }
        });
};

//note: use of stop function to prevent animation build-ups if called repeatedly
//subtracting container height brings scrollTo position to container bottom
scrollUp = function () {
    $("#messagePanel").stop().animate({scrollTop: 0}, "slow");
};

scrollDown = function () {
    var scroller = $('#messagePanel');
    var height = scroller[0].scrollHeight - $(scroller).height();

    $(scroller).stop().animate({scrollTop: height}, "slow");
};

onTypeAddUser = function (input) {
    // $.ajax(
    //   {
    //     url: "/search/utilisateur/"+input.value,
    //     type: "GET",
    //     async: true,
    //     dataType : "json",
    //     success: function(users)
    //     {
    //       for(let i=0;i<Object.keys(users).length;i++)
    //       {
    //
    //         console.log(users[i].Nom+" "+users[i].Prénom)
    //       }
    //
    //     },
    //   error: function(req, status, err)
    //   {
    //     console.log("Erreur")
    //   }
    //  });
    console.log(input.value)
};
