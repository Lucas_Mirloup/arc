import csv
from _hashlib import pbkdf2_hmac
from binascii import hexlify
from datetime import datetime

from flask import render_template, request, session, redirect, url_for, make_response, jsonify
from flask_login import login_user, login_required, logout_user, current_user
# from flask_mail import Message
from flask_weasyprint import render_pdf, HTML

from .app import app, db
from .forms import ProjectTemplateForm, BookletTemplateForm, CASLoginForm, ProjectSearch, BookletSearch, ProjectForm
from .models import (ProjectTemplate, BookletTemplate, Image, get_promoted_projects, add, getMessages, send_message,
                     get_professors, Project, Booklet, User, Training, Department, get_user_cas, update_role_professor)


@app.route("/")
def home():
    """
    Shows the home window.
    """
    pp = get_promoted_projects()
    pp = [pp[i:i + 4] for i in range(0, len(pp), 4)]
    return render_template(
        "home.html",
        title="Accueil",
        promoted_projects=pp,  # 10 because n = 10, the values need to match

    )


@app.route("/login/", methods=("GET", "POST",))
def login():
    """
    Shows the connection page/login form and redirects to the home page.
    """
    form = CASLoginForm(request.form)
    if not form.is_submitted():
        form.next.data = request.args.get("next")
    if form.validate_on_submit():
        user = get_user_cas(form.cas_id.data)
        if user:
            login_user(user=user)
            return redirect(form.next.data or url_for("home"))
    return render_template(
        "login.html",
        form=form
    )


@app.route("/logout/")
@login_required
def logout():
    """
    Disconnects the user and redirects to the home page.
    """
    logout_user()
    return redirect(url_for("home"))


@app.route("/projets/", methods=("GET", "POST",))
def projets():
    """
    Displays all the different 'projets' set to public.
    :return:
    """

    if request.method == 'GET':

        form = ProjectSearch(request.args)
        projects = Project.query

        if form.name.data is not None:
            projects = projects.filter(Project.title.contains(form.name.data))

        if form.date_start.data is not None:
            projects = projects.filter(Project.creation_date >= form.date_start.data)

        if form.date_end.data is not None:
            projects = projects.filter(Project.creation_date <= form.date_end.data)

        if form.department.data is not None:
            try:
                id = int(form.department.data)
                projects = projects.filter(
                    Project.trainings.any(Training.department.has(Department.id == id)))
            except ValueError:
                pass

        if form.tutor.data is not None:
            projects = projects.filter(Project.tutors.any(User.full_name.contains(form.tutor.data)))

        projects = projects.all()
        dico = {}
        for project in projects:
            dico[project.id] = set()
            for training in project.trainings:
                dico[project.id].add(training.department.title)

        if request.method == "POST":
            return redirect("/livrets/edit/" + str(add.add_project_template(
                current_user.id)))  # TODO remplacer cette fct par celle d'ajout de livret dans la bd

        return render_template(
            "projets.html",
            title="Affichage des projets",
            form=form,
            projects=projects,
            dicoProjets=dico
        )


@app.route("/livrets/", methods=("GET", "POST",))
def livrets():
    """
    Displays all the different 'livrets' set to public.
    :return:
    """
    if request.method == 'GET':

        form = BookletSearch(request.args)
        booklets = Booklet.query

        if form.name.data is not None:
            booklets = booklets.filter(Booklet.title.contains(form.name.data))

        if form.date_start.data is not None:
            booklets = booklets.filter(Booklet.creation_date >= form.date_start.data)

        if form.date_end.data is not None:
            booklets = booklets.filter(Booklet.creation_date <= form.date_end.data)

        if form.department.data is not None:
            try:
                id = int(form.department.data)
                booklets = booklets.filter(
                    Booklet.projects.any(Project.trainings.any(Training.department.has(Department.id == id))))
            except ValueError:
                pass

        if form.creator.data is not None:
            booklets = booklets.filter(Booklet.user.has(User.full_name.contains(form.creator.data)))

        booklets = booklets.all()
        dico = {}
        for livret in booklets:
            for projet in livret.projects:
                dico[projet.project.id] = set()
                for training in projet.project.trainings:
                    dico[projet.project.id].add(training.department.title)
        return render_template(
            "livrets.html",
            title="Affichage des projets",
            form=form,
            booklets=booklets,
            dicoLivret=dico
        )


@app.route("/projet/<int:index>/", methods=("GET", "POST",))
def projet(index: int):
    """
    Displays the selected project.
    :param index: id of the project
    :return:
    """
    if current_user.is_authenticated and (
            current_user in Project.query.get(index).users or current_user in Project.query.get(index).tutors):
        return render_template(
            "project_template_1.html",
            title=Project.query.get(index).title,
            project_participants=Project.query.get(index).users,
            company_name=Project.query.get(index).customer,
            image1="http://news.mit.edu/sites/mit.edu.newsoffice/files/images/2016/MIT-Earth-Dish_0.jpg",
            image2="https://sample-videos.com/img/Sample-jpg-image-500kb.jpg",
            image3="https://www.w3schools.com/w3css/img_lights.jpg",
            first_paragraph="Premier paragraphe de la fiche. \nLorem ipsum dolor sit amet, consectetur adipiscing "
                            "elit, "
                            "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim "
                            "veniam, "
                            "quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",

        )
    return "Access denied"


@app.route("/projet/telecharger/<int:index>/")
def project_dl(index: int):
    """
    Download selected project.
    :param index: id of the project
    :return:
    """
    if current_user.is_authenticated and (
            current_user in Project.query.get(index).users or current_user in Project.query.get(index).tutors):
        if len(Project.query.get(index).images) > 0:
            html = render_template(
                "project_template_1.html",
                title=Project.query.get(index).title,
                project_participants=Project.query.get(index).users,
                company_name=Project.query.get(index).customer,
                image1=Project.query.get(index).images[0],
                image2=Project.query.get(index).images[1],
                image3=Project.query.get(index).images[2],
                paragraph=Project.query.get(index).description)
        else:
            html = render_template(
                "project_template_1.html",
                title=Project.query.get(index).title,
                project_participants=Project.query.get(index).users,
                company_name=Project.query.get(index).customer,
                paragraph=Project.query.get(index).description)
        return render_pdf(HTML(string=html))
    return "Access denied"


@app.route("/livret/<int:index>/", methods=("GET",))
def livret(index: int):
    """
    Displays the selected livret.
    :return:
    """
    return render_template(
        "not_implemented.html",
    )


@app.route("/projet/edit/<int:index>/", methods=("GET", "POST",))
def projet_edit(index: int):
    """
    Shows the page to edit the selected projet.
    :return:
    """

    if request.method == "GET":
        project = None
        if index > 0:
            project = Project.query.get(index)
            if project is None:
                return redirect(url_for("not_found", error="Projet inexistant"))

            form = ProjectForm(id=project.id, title=project.title, customer=project.customer,
                               description=project.description)
        else:
            form = ProjectForm(id=0)

        return render_template(
            "edition_projet.html",
            project=project,
            title="Edition d'un projet",
            form=form,
            idUser=current_user.id
        )
    elif request.method == "POST":
        form = ProjectForm(request.form)
        if form.validate_on_submit():
            nb_student = int(form.nb_student.data)
            nb_tutor = int(form.nb_tutor.data)

            students = []
            for i in range(1, nb_student + 1):
                k = "etudiantAjoute" + str(i)
                if k in request.form:
                    id = request.form[k]
                    if len(id) > 0:
                        students.append(get_user_cas(id))

            tutors = []
            for i in range(1, nb_tutor + 1):
                k = "profTuteur" + str(i)
                if k in request.form:
                    id = request.form[k]
                    if len(id) > 0:
                        tutors.append(get_user_cas(id))

            if index <= 0:
                students.append(User.query.get(current_user.id))
                if len(tutors) <= 0:
                    return render_template(
                        "edition_projet.html",
                        title="Edition d'un projet",
                        project=None,
                        form=form,
                        idUser=current_user.id,
                        tutor_missing="Veuillez ajouter un tuteur à votre projet"
                    )

            if len(students) > 0:
                db.session.add_all(students)
                db.session.commit()
            if len(tutors) > 0:
                db.session.add_all(tutors)
                db.session.commit()

            if index <= 0:
                id_template = ProjectTemplate.query.first().id
                project = Project(title=form.title.data, customer=form.customer.data, description=form.description.data,
                                  creation_date=datetime.now(), id_project_template=id_template)
                if len(students) > 0:
                    project.tutors.extend(tutors)
                if len(tutors) > 0:
                    project.users.extend(students)
                db.session.add(project)
            else:
                project = Project.query.get(index)
                project.title = form.title.data
                project.customer = form.customer.data
                project.description = form.description.data
                project.tutors.extend(tutors)
                project.users.extend(students)

            db.session.commit()

            k = "customer_img"

            if k in request.files:
                img_client = Image(image=request.files[k].read(), project_id_client=project.id)
                db.session.add(img_client)
                db.session.commit()
            keys = ["image1", "image2", "image3"]
            imgs = []
            for k in keys:
                if k in request.files:
                    imgs.append(Image(image=request.files[k].read(), project_id=project.id))
            db.session.add_all(imgs)
            db.session.commit()

            return redirect("projets")

        else:
            return render_template(
                "edition_projet.html",
                title="Edition d'un projet",
                project=None,
                form=form,
                idUser=current_user.id
            )


@app.route("/projet/get_messages/<int:index>/", methods=("GET",))
def projet_edit_get_messages(index: int):
    """
    Get the suggestions for a given project
    :return:
    """
    suggestions = getMessages(index)
    dico = {}
    i = 0
    for sugg in suggestions:
        dico[i] = {"jour": sugg.date.day, "mois": sugg.date.month, "annee": sugg.date.year, "heure": sugg.date.hour,
                   "minute": sugg.date.minute, "seconde": sugg.date.second, "Contenu": sugg.suggestion,
                   "id_auteur": sugg.user_id, "fname": sugg.user.f_name, "lname": sugg.user.l_name}
        i += 1
    print(jsonify(dico))
    return jsonify(dico)


@app.route("/projet/edit/<int:index>/send_message/", methods=("POST",))
def projet_send_message(index: int):
    """
    Send a suggestion/message to the selected projet.
    :return:
    """

    json_data = request.get_json(force=True)
    send_message(index, json_data[0], json_data[1])
    resp = jsonify(success=True)
    return resp


@app.route("/livret/edit/<int:index>/", methods=("GET", "POST",))
def livret_edit(index: int):
    """
    Shows the page to edit the selected livret.
    :return:
    """
    return render_template(
        "edition_livret.html", projects=Project.query.all()
    )


@app.route("/maquettes/projets/", methods=("POST", "GET",))
def maquettes_projets():
    """
    Shows all the different templates of projets.
    :return:
    """
    if current_user.is_authenticated and (
            1 in current_user.get_all_roles_ids_cur_year() or 2 in current_user.get_all_roles_ids_cur_year()):
        if request.method == "POST":
            return redirect("/maquette/projet/edit/" + str(add.add_project_template(current_user.id)))

        return render_template(
            "maquettes_projets.html", project_template=ProjectTemplate.query.all()
        )
    return "Access denied"


@app.route("/maquettes/livrets/", methods=("POST", "GET",))
def maquettes_livrets():
    """
    Shows all the different templates of livrets.
    :return:
    """
    if current_user.is_authenticated and (
            1 in current_user.get_all_roles_ids_cur_year() or 2 in current_user.get_all_roles_ids_cur_year()):
        if request.method == "POST":
            return redirect("/maquette/livret/edit/" + str(add.add_booklet_template(current_user.id)))
        return render_template(
            "maquettes_livrets.html",
        )
    return "Access denied"


@app.route("/maquette/projet/edit/<int:index>/", methods=("GET", "POST",))
def maquette_projet_edit(index: int):
    """
    Shows the page to edit the selected template of projet.
    :return:
    """
    if current_user.is_authenticated and (
            1 in current_user.get_all_roles_ids_cur_year() or 2 in current_user.get_all_roles_ids_cur_year()):
        # A compléter avec le template une fois le modèle terminé
        project_template = ProjectTemplate.query.get(index)
        if project_template is None:
            return redirect(url_for('not_found'))
        form = ProjectTemplateForm(id=project_template.id,
                                   title=project_template.name,
                                   template=project_template.template)
        if request.method == 'POST':
            if form.validate_on_submit():
                project_template.name = form.title.data
                project_template.project_template = form.template.data
                db.session.commit()
                return redirect(url_for('maquettes_projets'))
        return render_template(
            "project_template_edit.html",
            template=project_template,
            form=form
        )
    return "Access denied"


@app.route("/maquette/projet/<int:index>")
def maquette_projet_afficher(index: int):
    """
        Shows the chosen project template
        :param index: index of the project template
        :return:
        """
    if current_user.is_authenticated and (
            1 in current_user.get_all_roles_ids_cur_year() or 2 in current_user.get_all_roles_ids_cur_year()):
        return render_template(
            "project_template_" + str(index) + ".html",
            title="Titre du projet",
            project_participants=["Liste des participants du projets"],
            company_name="Nom de l'entrprise qui a demandé le projet",
            paragraph="Paragraphe de la fiche. \nLorem ipsum dolor sit amet, consectetur adipiscing elit, "
                      "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, "
                      "quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.")
    return "Access denied"


@app.route("/maquettes/projets/telecharger/<int:index>")
def maquette_projet_dl(index: int):
    """
    Creates and download the PDF version of this project's model
    :param index: index of the model
    :return:
    """
    if current_user.is_authenticated and (
            1 in current_user.get_all_roles_ids_cur_year() or 2 in current_user.get_all_roles_ids_cur_year()):
        html = render_template(
            "project_template_" + str(index) + ".html",
            title="Titre du projet",
            project_participants=["Liste des participants du projets"],
            company_name="Nom de l'entrprise qui a demandé le projet",
            paragraph="Paragraphe de la fiche. \nLorem ipsum dolor sit amet, consectetur adipiscing elit, "
                      "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, "
                      "quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            image1="https://cdn.discordapp.com/attachments/489532818490392596/558406690794635275/unknown.png",
            image2="https://cdn.discordapp.com/attachments/489532818490392596/558406769433378826/unknown.png",
            image3="https://cdn.discordapp.com/attachments/489532818490392596/558406844830449674/unknown.png")
        return render_pdf(HTML(string=html))
    return "Access denied"


@app.route("/maquette/livret/edit/<int:index>/", methods=("POST", "GET",))
def maquette_livret_edit(index: int):
    """
    Shows the page to edit the selected template of livrets.
    :return:
    """
    if current_user.is_authenticated and (
            1 in current_user.get_all_roles_ids_cur_year() or 2 in current_user.get_all_roles_ids_cur_year()):
        booklet_template = BookletTemplate.query.get(index)
        if booklet_template is None:
            return redirect(url_for('not_found'))
        form = BookletTemplateForm(id=booklet_template.id,
                                   title=booklet_template.name,
                                   cover_template=booklet_template.cover,
                                   editorial_template=booklet_template.editorial,
                                   project_template=booklet_template.template)
        if request.method == 'POST':
            if form.validate_on_submit():
                booklet_template.name = form.title.data
                booklet_template.cover_template = form.cover_template.data
                booklet_template.editorial_template = form.editorial_template.data
                booklet_template.project_template = form.project_template.data
                db.session.commit()
                return redirect(url_for('maquettes_livrets'))
        return render_template(
            "booklet_template_edit.html",
            template=booklet_template,
            form=form
        )
    return "Access denied"


@app.route("/mesProjets/", methods=("GET",))
def mes_projets():
    """
    Displays all the different 'projets' set to public.
    :return:
    """

    if request.method == 'GET':

        form = ProjectSearch(request.args)
        projects = Project.query.filter(Project.users.any(User.id == current_user.id))

        if form.name.data is not None:
            projects = projects.filter(Project.title.contains(form.name.data))

        if form.date_start.data is not None:
            projects = projects.filter(Project.creation_date >= form.date_start.data)

        if form.date_end.data is not None:
            projects = projects.filter(Project.creation_date <= form.date_end.data)

        if form.department.data is not None:
            try:
                id = int(form.department.data)
                projects = projects.filter(
                    Project.trainings.any(Training.department.has(Department.id == id)))
            except ValueError:
                pass

        if form.tutor.data is not None:
            projects = projects.filter(Project.tutors.any(User.full_name.contains(form.tutor.data)))

        projects = projects.all()

        dico = {}
        for project in projects:
            dico[project.id] = set()
            for training in project.trainings:
                dico[project.id].add(training.department.title)

        return render_template(
            "mes_projets.html",
            title="Affichage de vos projets",
            form=form,
            projects=projects,
            dicoProjets=dico
        )


@app.route("/gestion/", methods=("GET",))
def gestion():
    """
    If the user is logged in, displays the administrator interface.
    Else, displays the login screen.
    """
    if not session.get('logged_in_admin'):
        return render_template('login_admin.html')
    return render_template("admin.html", profs=get_professors())


@app.route('/gestion/login', methods=['POST'])
def do_admin_login():
    """
    Backend
    Read admin.csv file and store data in the admins variable
    Check if the username and password combo is in admins
    If the combo is in admins the logged session variable is on True
    """
    with open('admin.csv') as csvFile:
        reader = csv.reader(csvFile, delimiter=',', quotechar='"')

        for row in reader:
            if row[0] == request.form['username'] and row[1] == hexlify(
                    pbkdf2_hmac('sha256', request.form['password'].encode(), app.secret_key.encode(), 100000)).decode():
                session['logged_in_admin'] = True
    return redirect(url_for('gestion'))


@app.route("/gestion/logout")
def do_admin_logout():
    """
    The logged session variable is turned on False
    Redirect to /gestion
    """
    session['logged_in_admin'] = False
    return redirect(url_for('gestion'))


@app.route('/gestion/background_process_staff', methods=['POST'])
def background_process_staff():
    """
    Backend
    Get the id and the choice chosen by the admin and call the function to update the database
    """
    update_role_professor(request.form['id'], int(request.form['choice']), int(request.form['dptid']))
    return redirect("/gestion")


# @app.route("/gestion/manage", methods=("GET",))
# def gestion_admins():
#     """
#     Backend
#     Coming soon
#     """
#     try:
#         msg = Message("Send Mail Tutorial!",
#                       sender="yoursendingemail@gmail.com",
#                       recipients=["recievingemail@email.com"])
#         msg.body = "Yo!\nHave you heard the good word of Python???"
#         mail.send(msg)
#         return 'Mail sent!'
#     except Exception as e:
#         return str(e)


@app.route('/image/<id>')
def get_image(id):
    image = Image.query.get(id)
    if image is None:
        return not_found("Image inexistante")
    response = make_response(image.image)
    response.headers.set('Content-Type', 'image/jpeg')
    response.headers.set(
        'Content-Disposition', 'attachment', filename='%s.jpg' % id)
    return response


@app.errorhandler(404)
def not_found(error):
    """
    Show this page whenever the url does not match any route.
    """
    return render_template(
        "404.html"
    ), 404
