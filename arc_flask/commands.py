import csv
from _hashlib import pbkdf2_hmac
from binascii import hexlify
from datetime import date, datetime
from shutil import move
from sys import stderr
from tempfile import NamedTemporaryFile

import click

from .app import app, db
from .models import User, Project, ProjectTemplate, UnivYear, Role, Training, Department, \
    HaveRoleInDepartment, update_role_professor, Suggest


@app.cli.command()
def resetdb():
    """Deletes all existing tables then creates tables according to models."""
    db.drop_all()
    db.create_all()
    Role.generate_roles()
    Department.generate_departments()


@app.cli.command()
def syncdb():
    """Creates missing tables."""
    db.create_all()


@app.cli.command()
def testadmin():
    """Tests associations."""
    db.drop_all()
    db.create_all()
    db.session.add(User(id_univ=42, f_name='Lucas', l_name='Mirloup'))
    db.session.add(Role(name="1"))
    db.session.add(Role(name="2"))
    dep = Department(code="CODE123", title="TestDep")
    db.session.add(dep)
    db.session.add(Training(name="TestTrain", department=dep))
    db.session.commit()
    update_role_professor(42, 2, 1)


@app.cli.command()
def testprojectlisting():
    """Populates the database to test the project listing"""
    prof = Role(name="Enseignant")
    etu = Role(name="Etudiant")
    db.session.add_all([prof, etu])
    db.session.commit()

    limet = User(id_univ="pppp",
                 f_name="Sébastien",
                 l_name="Limet",
                 email="seb5@mail.fr")

    thomas = User(id_univ="oooo",
                  f_name="Thomas",
                  l_name="Quetier",
                  email="thomas@mail.fr")
    db.session.add_all([limet, thomas])
    db.session.commit()

    annee = UnivYear(start_year=2018)

    dep = Department(code="bidon", title="Informatique")
    db.session.add_all([annee, dep])
    db.session.commit()

    info = Training(name="DUT2 Informatique", department_id=dep.id)

    db.session.add(info)
    db.session.commit()

    asso1 = HaveRoleInDepartment(user_id=limet.id, role_id=prof.id, department_id=info.id, univ_year=annee.start_year)
    asso2 = HaveRoleInDepartment(user_id=thomas.id, role_id=etu.id, department_id=info.id, univ_year=annee.start_year)

    template = ProjectTemplate(name="Projets 2019",
                               template="Test",
                               id_user=limet.id)

    db.session.add_all([asso1, asso2, template])
    db.session.commit()

    project = Project(title="ARC",
                      customer="IUT Orléans",
                      description="Super Projet",
                      creation_date=date(year=2018, month=9, day=1),
                      id_project_template=template.id)
    project.users.append(limet)
    project.users.append(thomas)
    project.trainings.append(info)

    db.session.add(project)
    db.session.commit()


@app.cli.command()
def populatesugg():
    """Populates the database to test the suggestion in project page"""

    suggestion = Suggest(user_id=2,
                         project_id=1,
                         suggestion="Bonjour Loris je test les suggestions.",
                         date=datetime(year=2018, month=9, day=1, hour=18, minute=42, second=49))
    suggestion2 = Suggest(user_id=1,
                          project_id=1,
                          suggestion="Bonjour Loris je test les suggestions deuxième message, ce message est tres tres tres tres long",
                          date=datetime(year=2018, month=9, day=1, hour=18, minute=52, second=49))

    db.session.add(suggestion)
    db.session.add(suggestion2)
    db.session.commit()


@app.cli.command()
@click.pass_context
@click.argument('username')
@click.argument('password')
def addadmin(ctx, username: str, password: str):
    """Crée un administrateur avec le nom et le mot de passe donnés."""
    with open('admin.csv') as csv_file:
        reader = csv.reader(csv_file, delimiter=',', quotechar='"')

        for row in reader:
            if row[0] == username:
                ctx.forward(changeadminpassword)
                return

    with open('admin.csv', 'a', newline='') as csv_file:
        row = [username, hexlify(pbkdf2_hmac('sha256', password.encode(), app.secret_key.encode(), 100000)).decode()]
        writer = csv.writer(csv_file, delimiter=',', quotechar='"')
        writer.writerow(row)


@app.cli.command()
@click.argument('username')
@click.argument('password')
def changeadminpassword(username: str, password: str):
    """Change le mot de passe d'un administrateur."""
    filename = 'admin.csv'
    tempfile = NamedTemporaryFile(delete=False, mode='w')

    exists = False

    with open(filename) as csv_file, tempfile:
        reader = csv.reader(csv_file, delimiter=',', quotechar='"')
        writer = csv.writer(tempfile, delimiter=',', quotechar='"')

        for row in reader:
            if row[0] == username:
                row[1] = hexlify(pbkdf2_hmac('sha256', password.encode(), app.secret_key.encode(), 100000)).decode()
                exists = True
            writer.writerow(row)

    if exists:
        print("Mot de passe de l'administrateur '" + username + "' changé.")
    else:
        print("Cet administrateur n'existe pas, si vous souhaitez en créer un, utilisez la commande Flask addadmin().",
              sep=' ', end='\n', file=stderr, flush=False)

    move(tempfile.name, filename)
