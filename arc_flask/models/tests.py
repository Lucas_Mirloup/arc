from arc_flask.app import app
from .base import *


@app.cli.command()
def testdb():
    db.drop_all()
    db.create_all()

    to_commit = []

    users = (User(id_univ=123456, f_name="Student 1", l_name="Test", email="s1@gmail.com"),
             User(id_univ=234567, f_name="Student 2", l_name="Test", email="s2@gmail.com"),
             User(id_univ=424242, f_name="Teacher 1", l_name="Test", email="t1@gmail.com"))
    to_commit.extend(users)

    Role.generate_roles()
    roles = Role.roles
    Department.generate_departments()
    departments = Department.departments

    db.session.add_all(to_commit)
    db.session.commit()
    to_commit = []

    training = Training(name="Info 2A", department_id=departments[0].id)

    to_commit.append(training)

    univ_year = UnivYear(start_year=2018)
    to_commit.append(univ_year)

    db.session.add_all(to_commit)
    db.session.commit()
    to_commit = []

    have_roles = [
        HaveRoleInDepartment(user_id=users[0].id, role_id=roles[0].id, department_id=departments[0].id,
                             univ_year=univ_year.start_year),
        HaveRoleInDepartment(user_id=users[1].id, role_id=roles[0].id, department_id=departments[0].id,
                             univ_year=univ_year.start_year),
        HaveRoleInDepartment(user_id=users[2].id, role_id=roles[1].id, department_id=departments[0].id,
                             univ_year=univ_year.start_year)]
    to_commit.extend(have_roles)

    projects = [Project(title="Project 1", customer="IUT", description="", creation_date=datetime.now()),
                Project(title="Project 2", customer="IUT", description="", creation_date=datetime.now()),
                Project(title="Project 3", customer="IUT", description="", creation_date=datetime.now()),
                Project(title="Project 4", customer="IUT", description="", creation_date=datetime.now())]

    project_template = ProjectTemplate(name="Project Template", template="", id_user=users[2].id, projects=projects)
    to_commit.append(project_template)

    projects[0].users.extend(users[:2])
    projects[0].tutors.append(users[2])
    projects[1].users.append(users[0])
    projects[0].trainings.append(training)
    projects[1].trainings.append(training)
    to_commit.extend(projects)

    db.session.add_all(to_commit)
    db.session.commit()

    booklet_template = BookletTemplate(id=1, name="maquette1", cover=" ", editorial="", template="", id_user=3)

    booklets = [Booklet(title="Projet info 2019", creation_date=datetime.now(), pdf=b"",
                        editorial="Bonjour, je suis un édito.", id_user=3, id_booklet_template=1),
                Booklet(title="Projet info 2019 2", creation_date=datetime.now(), pdf=b"",
                        editorial="Bonjour, je suis un édito v2.", id_user=3, id_booklet_template=1)]

    to_commit.append(booklet_template)
    to_commit.extend(booklets)

    db.session.add_all(to_commit)
    db.session.commit()
    to_commit = []

    booklets[0].add_project(projects[0].id)
    booklets[0].add_project(projects[1].id)
    booklets[0].add_project(projects[2].id)
    booklets[0].add_project(projects[3].id)

    booklets[1].add_project(projects[1].id)
    booklets[1].add_project(projects[2].id)

    to_commit.extend(booklets)
    db.session.add_all(to_commit)
    db.session.commit()
    to_commit = []

    booklets[0].remove_project(projects[1].id)
    booklets[0].move_project_after(projects[0].id)

    to_commit.extend(booklets)
    db.session.add_all(to_commit)
    db.session.commit()
