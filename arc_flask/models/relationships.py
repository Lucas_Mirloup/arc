from sqlalchemy.dialects.mysql import YEAR

from arc_flask.app import db

contribute = db.Table('contribute', db.Model.metadata,
                      db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
                      db.Column('project_id', db.Integer, db.ForeignKey('project.id')))

coordinate = db.Table('coordinate', db.Model.metadata,
                      db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
                      db.Column('project_id', db.Integer, db.ForeignKey('project.id')))


class Suggest(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    suggestion = db.Column(db.Text)
    date = db.Column(db.DateTime)

    user = db.relationship('User', back_populates='projects_suggestions')
    project = db.relationship('Project', back_populates='users_suggestions')


project_belongs_training = db.Table('project_belongs_training', db.Model.metadata,
                                    db.Column('department_id', db.Integer, db.ForeignKey('training.id')),
                                    db.Column('project_id', db.Integer, db.ForeignKey('project.id')))


class HaveRoleInDepartment(db.Model):
    # TODO: Set nullable(s) to False
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True, nullable=True)
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'), nullable=True)
    department_id = db.Column(db.Integer, db.ForeignKey('department.id'), primary_key=True, nullable=True)
    univ_year = db.Column(YEAR, db.ForeignKey('univ_year.start_year'), primary_key=True, nullable=True)


class BookletContainsProject(db.Model):
    booklet_id = db.Column(db.Integer, db.ForeignKey('booklet.id'), primary_key=True)
    project_id = db.Column(db.Integer, db.ForeignKey('project.id'), primary_key=True)
    order = db.Column(db.Integer, primary_key=True)

    booklet = db.relationship('Booklet', back_populates='projects')
    project = db.relationship('Project', back_populates='booklets')
