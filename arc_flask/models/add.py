from datetime import datetime

from arc_flask.app import db
from .base import Project, ProjectTemplate, BookletTemplate


def add_project(title: str, description: str, customer: str = None):
    """
    Adds a project to the database
    :param description: description of the project as a string
    :param customer: name of the customer (company name or CEO name) as a string
    :param title: title of the project as a string
    """
    Project.insert().values(title=title, customer=customer, description=description, creation_date=datetime.now())


def add_project_template(id_user):
    pt = ProjectTemplate(name="Nouvelle Maquette", template="", id_user=id_user)
    db.session.add(pt)
    db.session.commit()
    return pt.id


def add_booklet_template(id_user):
    bt = BookletTemplate(name="Nouvelle Maquette", cover="", editorial="", template="", id_user=id_user)
    db.session.add(bt)
    db.session.commit()
    return bt.id
