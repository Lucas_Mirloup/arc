from .base import Project


def get_project_infos(project_id):
    """
    This function returns all infos of the project corresponding to the project ID given in parameter
    :return:
    """
    return Project.query.filter(Project.id == project_id).order_by(Project.creation_date)


def get_project_infos_dict(index: int):
    """
    Get the infos as a dictionary for a given project
    :return:
    """
    project_infos = get_project_infos(index)
    dico = {}
    for project in project_infos:
        dico = {
            "id": project.id, "titre": project.title, "client": project.customer, "description": project.description,
            "valide": project.is_validated, "promu": project.is_promoted, "template": project.id_project_template
        }
        dico["idImages"] = []
        for image in project.images:
            dico["idImages"].append(image.id)
        dico["idUtilisateurs"] = []
        for utilisateur in project.users:
            dico["idUtilisateurs"].append(utilisateur.id)
    return dico
