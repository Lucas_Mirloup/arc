import datetime

from arc_flask.app import db
from .relationships import Suggest


def getMessages(project_id):
    """
    This function get the messages (suggestion) for a given project id
    """
    return Suggest.query.filter(Suggest.project_id == project_id).order_by(Suggest.date)


def send_message(projectid, userid, message):
    """
    This function sends a message (suggestion) for a given project id and user id
    """
    suggestion = Suggest(user_id=userid,
                         project_id=projectid,
                         suggestion=message,
                         date=datetime.datetime.now())
    db.session.add(suggestion)
    db.session.commit()
