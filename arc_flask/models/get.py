from .base import Project, Booklet, User


def get_projects_order_date():
    """
    This function returns all existing projects of the database
    :return: return a
    """
    return Project.query.all().order_by(Project.creation_date.desc())


def get_booklets_order_id():
    """
    Returns all booklets from the database
    :return:
    """
    return Booklet.query.all().order_by(Booklet.id)


def get_user(nom):
    users = User.query.filter(User.full_name.contains(nom)).all()
    return users
