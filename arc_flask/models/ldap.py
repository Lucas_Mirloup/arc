import datetime

import ldap3

from arc_flask.app import db
from .base import Department, User, HaveRoleInDepartment

try:
    ldap_server = ldap3.Server("ldaps://ldap-univ.iut45.univ-orleans.fr:636")
    ldap_connection = ldap3.Connection(ldap_server, client_strategy=ldap3.ASYNC)

    assert ldap_connection.bind()
except:
    # TODO: remove try/except and FakeLDAP class
    class FakeLDAP:
        def search(self, *args, **kwargs):
            return args[1]

        def get_response(self, *args, **kwargs):
            return [[{"attributes": {"givenName": "Prénom", "sn": "Nom", "mail": args[0] + "@univ-orleans.fr",
                                     "unrcEtape": "RU2I41", "supannEntiteAffectation": ["905E"]}}]]


    ldap_connection = FakeLDAP()

with open("departments.csv", "r") as dpts:
    departments = {}
    for department in dpts:
        name, code = department.strip().split(",")
        departments[code] = name

with open("trainings.csv") as trs:
    trainings = {}
    for training in trs:
        name, year, code = training.strip().split(",")
        trainings[code] = name, year


def get_user_cas(cas_id: str) -> User:
    """
    Gives the User corresponding to the CAS ID provided, and inserts it in the database if it doesn't exist.
    :param cas_id: CAS ID of the wanted User
    :return: User corresponding to the CAS ID
    """
    year = datetime.date.today().year
    if datetime.date.today().month < 9:
        year -= 1

    if db.session.query(User.id).filter_by(id_univ=cas_id).first() is not None:
        user = db.session.query(User.id).filter_by(id_univ=cas_id).first()
        if db.session.query(HaveRoleInDepartment).filter_by(user_id=user.id, univ_year=year).first() is None:
            if cas_id[0] == "p":
                role = HaveRoleInDepartment(user_id=user.id, role_id=3, department_id=get_department_user(user).id,
                                            univ_year=year)
                db.session.add(role)
                db.session.commit()
        return User.query.filter(User.id_univ == cas_id).first()
    else:
        req = ldap_connection.search("ou=People,dc=univ-orleans,dc=fr", "(uid=" + cas_id + ")",
                                     attributes=["sn", "givenName", "mail"])
        data = ldap_connection.get_response(req)[0][0]["attributes"]
        user = User(id_univ=cas_id, f_name=data["givenName"], l_name=data["sn"], email=data["mail"])
        db.session.add(user)
        db.session.commit()
        if cas_id[0] == "p":
            role = HaveRoleInDepartment(user_id=user.id, role_id=3, department_id=get_department_user(user).id,
                                        univ_year=year)
            db.session.add(role)
            db.session.commit()
        return user


def get_department_user(user: User):
    req = ldap_connection.search("ou=People,dc=univ-orleans,dc=fr", "(uid=" + user.id_univ + ")",
                                 attributes=["unrcEtape", "supannEntiteAffectation"])
    data = ldap_connection.get_response(req)[0][0]["attributes"]
    if user.id_univ.startswith("p"):
        for code in data["supannEntiteAffectation"]:
            return db.session.query(Department).filter_by(title=departments[code]).first()
    return db.session.query(Department).filter_by(title=trainings[data["unrcEtape"]]).first()
