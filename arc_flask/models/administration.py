import datetime

from arc_flask.app import db
from .base import User
from .ldap import get_department_user
from .relationships import HaveRoleInDepartment


def get_professors():
    profs = db.session.query(User).filter(User.id_univ.like('p%')).order_by('l_name').all()
    res = []
    for p in profs:
        id_p = p.id
        id_role = \
            db.session.query(HaveRoleInDepartment.role_id).filter(HaveRoleInDepartment.user_id.like(id_p)).first()[0]
        res.append((p, id_role, get_department_user(p).title, get_department_user(p).id))
    return res


def update_role_professor(staff_id, new_role, department_id):
    year = datetime.date.today().year
    if datetime.date.today().month < 9:
        year -= 1

    staff = db.session.query(User).filter_by(id=staff_id).first()
    role = db.session.query(HaveRoleInDepartment).filter_by(user_id=staff.id, department_id=department_id,
                                                            univ_year=year).first()

    if new_role == 1:  # id leaders
        if db.session.query(HaveRoleInDepartment).filter_by(role_id=new_role,
                                                            department_id=department_id).first() is not None:
            old_leader = db.session.query(HaveRoleInDepartment).filter_by(role_id=new_role,
                                                                          department_id=department_id).first()
            old_leader.role_id = 3  # id simple professor
            db.session.commit()
        role.role_id = new_role
        db.session.commit()

    else:
        role.role_id = new_role
        db.session.flush()
        db.session.commit()
