from datetime import datetime

from sqlalchemy.dialects.mysql import MEDIUMBLOB, YEAR
from sqlalchemy.ext.hybrid import hybrid_property

from arc_flask.app import db
from .relationships import HaveRoleInDepartment, contribute, project_belongs_training, BookletContainsProject, \
    coordinate


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_univ = db.Column(db.String(10), unique=True, nullable=False)
    f_name = db.Column(db.String(255), nullable=False)
    l_name = db.Column(db.String(255), nullable=False)
    # The maximum length allowed for an UNIQUE or PRIMARY String is 191
    email = db.Column(db.String(191), unique=True, nullable=True)

    projects = db.relationship('Project', secondary=contribute, back_populates='users')
    tutored_projects = db.relationship('Project', secondary=coordinate, back_populates='tutors')
    projects_suggestions = db.relationship('Suggest', back_populates='user')
    booklets = db.relationship('Booklet', back_populates='user')
    booklet_templates = db.relationship('BookletTemplate', back_populates='user')
    project_templates = db.relationship('ProjectTemplate', back_populates='user')

    is_authenticated = True
    is_active = True
    is_anonymous = True

    def __repr__(self):
        return '<User ({}) {} {}>'.format(self.id, self.f_name, self.l_name)

    @hybrid_property
    def full_name(self) -> str:
        return self.f_name + " " + self.l_name

    def get_id(self):
        return self.id_univ

    def get_user_roles(self, department_id: int, univ_year: int):
        return db.session.query(Role).join(HaveRoleInDepartment).filter(
            HaveRoleInDepartment.department_id == department_id,
            HaveRoleInDepartment.user_id == self.id,
            HaveRoleInDepartment.univ_year == univ_year).all()

    def get_all_roles_ids_cur_year(self):
        year = datetime.now().year if datetime.now().month >= 9 else datetime.now().year - 1
        return [x.id for x in db.session.query(Role).join(HaveRoleInDepartment).filter(
            HaveRoleInDepartment.user_id == self.id,
            HaveRoleInDepartment.univ_year == year).all()]


class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    customer = db.Column(db.String(255), nullable=True)
    description = db.Column(db.String(255), nullable=False)
    creation_date = db.Column(db.DateTime, nullable=False)
    is_validated = db.Column(db.Boolean, nullable=False, default=False)
    is_promoted = db.Column(db.Boolean, nullable=False, default=False)

    users = db.relationship('User', secondary=contribute, back_populates='projects')
    tutors = db.relationship('User', secondary=coordinate, back_populates='tutored_projects')
    images = db.relationship('Image', back_populates='project', foreign_keys="[Image.project_id]")
    image_client = db.relationship('Image', uselist=False, back_populates='project_client',
                                   foreign_keys="[Image.project_id_client]")
    users_suggestions = db.relationship('Suggest', back_populates='project')
    id_project_template = db.Column(db.Integer, db.ForeignKey('project_template.id'), nullable=False)
    project_template = db.relationship('ProjectTemplate', back_populates='projects')
    trainings = db.relationship('Training', secondary=project_belongs_training, back_populates='projects')
    booklets = db.relationship('BookletContainsProject', back_populates='project')

    def __repr__(self):
        return '<Project ({}) {}>'.format(self.id, self.title)

    def update_project(self, title, client, students_list, tutors_list, images_list, client_image, description):
        Project.update(Project.id == self.id).values(title=title, customer=client, users=students_list,
                                                     tutors=tutors_list, images=images_list, image_client=client_image,
                                                     description=description)
        db.commit()


class Image(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    image = db.Column(MEDIUMBLOB, nullable=False)

    project_id = db.Column(db.Integer, db.ForeignKey('project.id'), nullable=True)
    project = db.relationship("Project", back_populates="images", foreign_keys=[project_id])
    project_id_client = db.Column(db.Integer, db.ForeignKey('project.id'), nullable=True)
    project_client = db.relationship("Project", back_populates="image_client", foreign_keys=[project_id_client])


class Training(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)

    projects = db.relationship('Project', secondary=project_belongs_training, back_populates='trainings')
    department_id = db.Column(db.Integer, db.ForeignKey('department.id'), nullable=True)
    department = db.relationship("Department", back_populates="trainings")


class Department(db.Model):
    departments = None

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(20), unique=True, nullable=False)

    trainings = db.relationship('Training', back_populates='department')

    @staticmethod
    def generate_departments():
        db.session.add_all([Department(title='GEA'), Department(title='GMP'), Department(title='GTE'),
                            Department(title='QLIO'), Department(title='Informatique'), Department(title='Chimie')])
        db.session.commit()
        Department.departments = Department.query.order_by(Department.id).all()


class Role(db.Model):
    roles = None

    id = db.Column(db.Integer, primary_key=True)
    # The maximum length allowed for an UNIQUE or PRIMARY String is 191
    name = db.Column(db.String(191), unique=True, nullable=False)

    @staticmethod
    def generate_roles():
        db.session.add_all([Role(name="Chef de département"), Role(name="Service Communication"),
                            Role(name="Professeur"), Role(name="Étudiant")])
        db.session.commit()
        Role.roles = Role.query.order_by(Role.id).all()


class UnivYear(db.Model):
    start_year = db.Column(YEAR, primary_key=True)


class Booklet(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(255), nullable=False)
    creation_date = db.Column(db.DateTime, nullable=False)
    pdf = db.Column(db.LargeBinary(), nullable=False)
    editorial = db.Column(db.Text(), nullable=False)

    id_user = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', back_populates='booklets')
    id_booklet_template = db.Column(db.Integer, db.ForeignKey('booklet_template.id'), nullable=False)
    booklet_templates = db.relationship('BookletTemplate', back_populates='booklets')
    projects = db.relationship('BookletContainsProject', back_populates='booklet')

    def get_projects(self):
        return [x.project for x in BookletContainsProject.query.filter_by(booklet_id=self.id).order_by('order').all()]

    def add_project(self, project_id: int):
        order = 1
        if self.projects:
            order = max([x.order for x in self.projects]) + 1
        db.session.add(BookletContainsProject(booklet_id=self.id, project_id=project_id, order=order))
        db.session.commit()

    def remove_project(self, project_id: int):
        db.session.delete(BookletContainsProject.query.filter_by(booklet_id=self.id, project_id=project_id).first())
        projects = BookletContainsProject.query.filter_by(booklet_id=self.id).order_by('order').all()
        for i in range(len(projects)):
            projects[i].order = i + 1
        db.session.commit()

    def move_project_before(self, project_id: int):
        order = BookletContainsProject.query.filter_by(booklet_id=self.id, project_id=project_id).first().order
        projects = BookletContainsProject.query.filter_by(booklet_id=self.id).order_by('order').all()
        length = len(projects)
        if order == 1:
            for i in range(1, length):
                projects[i].order = i
            projects[0].order = length
        else:
            projects[order - 1].order = order - 1
            projects[order - 2].order = order
        db.session.commit()

    def move_project_after(self, project_id: int):
        order = BookletContainsProject.query.filter_by(booklet_id=self.id, project_id=project_id).first().order
        projects = BookletContainsProject.query.filter_by(booklet_id=self.id).order_by('order').all()
        length = len(projects)
        if order == length:
            for i in range(length - 1):
                projects[i].order = i + 2
            projects[length - 1].order = 1
        else:
            projects[order - 1].order = order + 1
            projects[order].order = order
        db.session.commit()

    def edit_booklet(self, title: str = None, editorial: str = None):
        if title is not None:
            self.title = title
        if editorial is not None:
            self.editorial = editorial
        db.commit.add(self)


class BookletTemplate(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    cover = db.Column(db.Text(), nullable=False)
    editorial = db.Column(db.Text(), nullable=False)
    template = db.Column(db.Text(), nullable=False)

    id_user = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', back_populates='booklet_templates')
    booklets = db.relationship('Booklet', back_populates='booklet_templates')


class ProjectTemplate(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    template = db.Column(db.Text(), nullable=False)
    id_user = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    user = db.relationship('User', back_populates='project_templates')
    projects = db.relationship('Project', back_populates='project_template')
