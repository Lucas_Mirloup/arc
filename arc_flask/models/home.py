from .base import Project


def get_promoted_projects():
    """
    This function returns the n number of most recent promoted projects
    :param n: number of projects required
    :return: return the n first promoted projects
    """
    return Project.query.filter(Project.is_promoted == 1).order_by(Project.creation_date.desc()).all()
