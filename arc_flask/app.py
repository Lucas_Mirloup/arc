from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

# from flask_mail import Mail, Message


app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = "c2dea5cd-798b-4b26-8883-6f2093bf8702"
app.config['UPLOAD_FOLDER'] = "tmp"

# mail_settings = {
#     "MAIL_SERVER": 'smtp.office365.com:443',
#     "MAIL_PORT": 587,
#     "MAIL_USE_TLS": False,
#     "MAIL_USE_SSL": True,
#     "MAIL_USERNAME": "arc.iut45@outlook.fr",
#     "MAIL_PASSWORD": "ProjetArc2019"
# }
#
# app.config.update(mail_settings)
# mail = Mail(app)

# Reads database URI from URI.txt file
with open('URI.txt', 'r') as content_file:
    app.config['SQLALCHEMY_DATABASE_URI'] = content_file.read()
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

login_manager = LoginManager(app=app)


@login_manager.user_loader
def load_user(user_id):
    from .models import get_user_cas
    return get_user_cas(user_id)
