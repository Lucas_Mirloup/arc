from flask_wtf import FlaskForm
from wtforms import HiddenField, StringField, TextAreaField, SelectField, FileField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired

from .models import Department


class ProjectTemplateForm(FlaskForm):
    id = HiddenField("id")
    title = StringField("Titre", validators=[DataRequired("Veuillez entrer un titre de maquette")])
    template = TextAreaField("Maquette de fiche de projet")


class BookletTemplateForm(FlaskForm):
    id = HiddenField("id")
    title = StringField("Titre", validators=[DataRequired("Veuillez entrer un titre de maquette")])
    cover_template = TextAreaField("Maquette de page de garde")
    editorial_template = TextAreaField("Maquette d'éditorial")
    project_template = TextAreaField("Maquette de fiche de projet")


class ProjectSearch(FlaskForm):
    name = StringField("Nom")
    date_start = DateField("Entre le")
    date_end = DateField("Et le")
    department = SelectField("Departement")
    tutor = StringField("Tuteur")

    def __init__(self, *args, **kwargs):
        super(ProjectSearch, self).__init__(*args, **kwargs)
        self.department.choices = [("", "Choisir un département")]
        self.department.choices.extend([(dep.id, dep.title) for dep in Department.query.all()])


class BookletSearch(FlaskForm):
    name = StringField("Nom")
    date_start = DateField("Entre le")
    date_end = DateField("Et le")
    department = SelectField("Departement")
    creator = StringField("Créateur")

    def __init__(self, *args, **kwargs):
        super(BookletSearch, self).__init__(*args, **kwargs)
        self.department.choices = [("", "Choisir un département")]
        self.department.choices.extend([(dep.id, dep.title) for dep in Department.query.all()])


class CASLoginForm(FlaskForm):
    cas_id = StringField("ID CAS", validators=[DataRequired("ID de connexion nécessaire")])
    next = HiddenField()


class ProjectForm(FlaskForm):
    id = HiddenField("id")
    nb_student = HiddenField("nb_student", default="1")
    nb_tutor = HiddenField("nb_tutor", default="1")
    title = StringField("Titre", validators=[DataRequired("Veuillez entrer un titre de projet")])
    customer = StringField("Client")
    description = TextAreaField("Description")
    image_customer = FileField("Logo client")
    image1 = FileField("Image 1")
    image2 = FileField("Image 2")
    image3 = FileField("Image 3")
